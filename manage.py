#!venv/bin/python
import sys

from flask_migrate import MigrateCommand
from flask_script import Command, Manager, Option

from webapp import init_app, create_app, socketio

mode = 'development'
if len(sys.argv) == 3 and sys.argv[1] == 'runserver' and sys.argv[2] == 'production':
    mode = 'production'
    del sys.argv[2]
    print 'prod!!'

app = create_app()

if len(sys.argv) > 1 and sys.argv[1] != 'runserver':
    init_app(app, mode)


class Server(Command):
    option_list = (
        Option('--debug-fe', dest='debug_fe', default=False),
        Option('--debug-tb', dest='debug_tb_enabled', default=False),
        Option('--ssl', dest='ssl', default=False),
    )

    def run(self, **kwargs):
        init_app(app, mode, **kwargs)
        if kwargs.get('ssl'):
            socketio.run(app, host='0.0.0.0', keyfile='provisioning/localhost.key',
                         certfile='provisioning/localhost.crt')
        else:
            socketio.run(app, host='0.0.0.0')


manager = Manager(app)
manager.add_command("runserver", Server())
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
