#!/bin/bash

echo "Install OS packages"
sudo apt update
sudo apt install -y git python-pip python-virtualenv python-dev nginx curl postgresql postgresql-contrib python-psycopg2 postgresql-server-dev-9.5 libffi-dev postgresql-9.5
sudo chown ubuntu:ubuntu /var/www

echo "Creating Python virtual environment"
virtualenv /var/www/app_venv
ln -s /var/www/app_venv /var/www/app/venv
cd /var/www/app
source venv/bin/activate
pip install --upgrade pip
pip install -r provisioning/requirements.txt

echo "Creating databases"
source sensitive.py
sudo -u postgres psql --command "CREATE USER app_user WITH PASSWORD '$ADMIN_DB_PASSWORD';"
sudo -u postgres psql --command "CREATE DATABASE app;"
sudo -u postgres psql --command "CREATE DATABASE app_test;"
sudo -u postgres psql --command "GRANT ALL PRIVILEGES ON DATABASE app to app_user;"
sudo -u postgres psql --command "GRANT ALL PRIVILEGES ON DATABASE app_test to app_user;"
export LC_CTYPE="en_US.UTF-8"
./manage.py db upgrade

echo "Installing frontend requirements"
mkdir -p /var/www/app/bin
wget --quiet https://nodejs.org/dist/v6.11.1/node-v6.11.1-linux-x64.tar.xz -O /var/www/app/bin/node.tar.xz
tar xfP /var/www/app/bin/node.tar.xz -C /var/www/app/bin
rm /var/www/app/bin/node.tar.xz
mv /var/www/app/bin/node-v6.11.1-linux-x64 /var/www/app/bin/node
export PATH="$PATH:/var/www/app/bin/node/bin"
npm install --prefix=. provisioning
mkdir -p webapp/static/js/vendor
mkdir -p webapp/static/css/vendor
cp node_modules/jquery/dist/jquery.js webapp/static/js/vendor/jquery.js
cp node_modules/underscore/underscore.js webapp/static/js/vendor/underscore.js
cp node_modules/backbone/backbone.js webapp/static/js/vendor/backbone.js
cp node_modules/backbone.localstorage/backbone.localStorage.js webapp/static/js/vendor/backbone.localStorage.js
cp node_modules/socket.io-client/dist/socket.io.js webapp/static/js/vendor/socket.io.js
cp node_modules/moment/moment.js webapp/static/js/vendor/moment.js
cp node_modules/materialize-css/dist/js/materialize.js webapp/static/js/vendor/materialize.js
cp node_modules/materialize-css/dist/css/materialize.css webapp/static/css/vendor/materialize.css
cp node_modules/bootstrap-calendar/css/calendar.css webapp/static/css/vendor/calendar.css
cp node_modules/bootstrap-calendar/js/calendar.js webapp/static/js/vendor/calendar.js
mkdir -p webapp/static/js/vendor/calendar
cp -r node_modules/bootstrap-calendar/tmpls webapp/static/js/vendor/calendar/

echo "Updating .bashrc"
echo "cd /var/www/app && source /var/www/app/venv/bin/activate" >> /home/ubuntu/.bashrc
echo "export LC_ALL=en_US.UTF-8" >> /home/ubuntu/.bashrc
echo 'export PATH="$PATH:/var/www/app/bin/node/bin"' >> /home/ubuntu/.bashrc

echo "Populating with initial models"
./populate.py
