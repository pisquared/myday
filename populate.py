#!venv/bin/python
import datetime
from dateutil import tz
from flask_security.utils import encrypt_password

import sensitive
from manage import app
from store import models, database
from webapp import init_app, get_now_user
from webapp.assets import DEFAULT_PANELS
from webapp.config import Constants

init_app(app, 'development')

with app.app_context():
    tzinfo = tz.tzoffset(None, datetime.timedelta(seconds=10800))

    admin_role = models.Role.create(name=Constants.ADMIN_ROLE)
    user = models.User.create(email='admin@daylog.com',
                              password=encrypt_password(sensitive.ADMIN_PASSWORD),
                              roles=[admin_role],
                              active=True,
                              name='Daniel',
                              tz_offset_seconds=10800)
    database.add(user)
    now = get_now_user(user)
    yesterday = now - datetime.timedelta(days=1)

    models.GeneralSettings.create(user=user,
                                  panels=DEFAULT_PANELS)

    ## TASKS ###
    models.Task.create(body="Task create from button",
                       status=models.Task._STATUS_DONE,
                       done_ts=now,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Task update",
                       status=models.Task._STATUS_DONE,
                       done_ts=now,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Task delete",
                       status=models.Task._STATUS_DONE,
                       done_ts=now,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Task run",
                       status=models.Task._STATUS_DONE,
                       done_ts=yesterday,
                       created_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Task pause",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Task done",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Journal task create",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Calendar date view",
                       status=models.Task._STATUS_IN_PROGRESS,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Calendar week view",
                       status=models.Task._STATUS_IN_PROGRESS,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Marketing front page",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Login/Register",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Journal entry create",
                       status=models.Task._STATUS_DONE,
                       created_ts=yesterday,
                       done_ts=yesterday,
                       start_ts=yesterday,
                       user=user)
    models.Task.create(body="Journal entry update",
                       status=models.Task._STATUS_NOT_STARTED,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Journal entry delete",
                       status=models.Task._STATUS_NOT_STARTED,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Journal entry link to task",
                       status=models.Task._STATUS_NOT_STARTED,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="BUG: Journal entry on create Invalid date",
                       status=models.Task._STATUS_DONE,
                       created_ts=now,
                       start_ts=now,
                       done_ts=now,
                       user=user)
    models.Task.create(body="Filter tasks by done ts",
                       status=models.Task._STATUS_DONE,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Vagrant setup password hide",
                       status=models.Task._STATUS_DONE,
                       done_ts=now,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Task create/update remove start_ts/due_ts",
                       status=models.Task._STATUS_DONE,
                       done_ts=now,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Journal entry edit button fix",
                       status=models.Task._STATUS_NOT_STARTED,
                       created_ts=now,
                       start_ts=now,
                       user=user)
    models.Task.create(body="Journal entry local save",
                       status=models.Task._STATUS_NOT_STARTED,
                       created_ts=now,
                       start_ts=now,
                       user=user)

    # ACTIVITIES
    programming_activity = models.Activity.create(name="Programming", icon="programming.png", user=user)
    sleeping_activity = models.Activity.create(name="Sleeping", icon="sleep.png", user=user)
    eating_activity = models.Activity.create(name="Eating", icon="eating.png", user=user)
    running_activity = models.Activity.create(name="Running", icon="running.png", user=user)
    walking_activity = models.Activity.create(name="Walking", icon="walking.png", user=user)
    meditating_activity = models.Activity.create(name="Meditating", icon="meditation.png", user=user)
    chat_activity = models.Activity.create(name="Chat", icon="chat.png", user=user)
    entertainment_activity = models.Activity.create(name="Entertainment", icon="entertainment.png", user=user)
    social_activity = models.Activity.create(name="Social", icon="social.png", user=user)
    shopping_activity = models.Activity.create(name="Shopping", icon="shopping.png", user=user)
    shower_activity = models.Activity.create(name="Shower", icon="shower.png", user=user)
    relationship_activity = models.Activity.create(name="Relationship", icon="relationship.png", user=user)
    news_activity = models.Activity.create(name="News", icon="news.png", user=user)
    cleaning_activity = models.Activity.create(name="Cleaning", icon="cleaning.png", user=user)
    washing_dishes_activity = models.Activity.create(name="Washing dishes", icon="washing_dishes.png", user=user)

    # LATEST CREATED/UPDATED/DELETED
    models.LatestCUD.create(user=user,
                            model_name='task',
                            created_ts=now)
    models.LatestCUD.create(user=user,
                            model_name='journal_entry',
                            created_ts=now)

    database.push()
