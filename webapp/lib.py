import json

import datetime
import pytz as pytz
import re
from dateparser import parse as human_dateparse
from dateutil import tz
from dateutil.parser import parse as dateparse
from flask import request, jsonify
from flask_login import login_required, current_user
from inflection import pluralize
from markupsafe import escape, Markup


def json_serialize(d):
    rv = {}
    for field, value in d.iteritems():
        try:
            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y-%m-%d %H:%M:%S%Z")
            json.dumps(value)
            rv[field] = value
        except TypeError:
            continue
    print rv
    return rv


def parse_dt_strategy(value, parser_f):
    dt = None
    if value:
        try:
            dt = parser_f(value).replace(tzinfo=get_user_tz(current_user))
        except TypeError, ValueError:
            dt = None
    return dt


def human_parse_dt(value):
    return parse_dt_strategy(value, human_dateparse)


def parse_dt(value):
    return parse_dt_strategy(value, dateparse)


def serialize_request_form(allowed_keys):
    rv = {}
    payload = request.form or request.json
    for k, v in payload.iteritems():
        if k not in ['csrf_token', ] and k in allowed_keys:
            if type(v) in [unicode, str, ] and v.isdigit():
                v = int(v)
            if '_ts' in k:
                v = parse_dt(v)
            rv[k] = v
    return rv


class ModelView(object):
    def __init__(self, name, model, form=None, creatable=True, editable=True, deletable=True, extra_actions=False):
        from store.models import LatestCUD
        self.LatestCUD = LatestCUD

        self.model = model
        self.name = name
        self.name_plural = pluralize(self.name)

        self.form = form
        self.creatable = creatable
        self.editable = editable
        self.deletable = deletable
        self.extra_actions = extra_actions

    def _get_form_include(self):
        default_include = ['request_sid'] + self.form.Meta.include
        return default_include + self.form.Meta.extra_include \
            if hasattr(self.form.Meta, 'extra_include') \
            else default_include

    def init_form(self, **kwargs):
        form_instance = self.form(**kwargs)
        if hasattr(self.form, 'choice_methods'):
            for choice_field, meth in self.form.choice_methods.iteritems():
                if type(meth) is str and meth.startswith('user.'):
                    choices = [(a.id, a.name) for a in getattr(current_user, meth.split('user.')[1])]
                else:
                    choices = meth
                getattr(form_instance, choice_field).choices = choices
        return form_instance

    def add_create(self, form_data):
        from store import database
        from webapp import socketio
        now = get_now_user(current_user)
        latest_cud = self.LatestCUD.get_or_create(user_id=current_user.id, model_name=self.name)
        latest_cud.created_ts = get_now_user(current_user)
        request_sid = form_data.pop('request_sid', None)
        instance = self.model.create(created_ts=now, user=current_user, **form_data)
        database.add(instance)
        database.push()
        rv = {'instance': instance.serialize_flat(), 'request_sid': request_sid}
        socketio.emit('{name}.created'.format(name=self.name), rv, room=current_user.id)
        return rv

    def add_update(self, instance, updated_elements):
        from store import database
        from webapp import socketio
        now = get_now_user(current_user)
        latest_cud = self.LatestCUD.get_or_create(user_id=current_user.id, model_name=self.name)
        latest_cud.updated_ts = now
        request_sid = updated_elements.pop('request_sid', None)
        database.update(instance, user_id=current_user.id, updated_ts=now, **updated_elements)
        database.push()
        rv = {'id': instance.id, 'updates': json_serialize(updated_elements), 'request_sid': request_sid}
        socketio.emit('{name}.updated'.format(name=self.name), rv, room=current_user.id)
        return rv

    def add_delete(self, instance):
        from store import database
        from webapp import socketio
        instance_id = instance.id
        latest_cud = self.LatestCUD.get_or_create(user_id=current_user.id, model_name=self.name)
        latest_cud.deleted_ts = get_now_user(current_user)
        database.delete(instance)
        database.push()
        request_sid = request.form.get('request_sid', None)
        rv = {'id': instance_id, 'request_sid': request_sid}
        socketio.emit('{name}.deleted'.format(name=self.name), rv, room=current_user.id)
        return rv

    def before_create(self, form_data):
        """override if needed - need to return form_data"""
        return form_data

    def handle_patch(self, instance, json):
        """override if needed - need to return updated elements"""
        request_sid = json.pop('request_sid', None)
        return {'request_sid': request_sid}

    def handle_get_filters(self, filters):
        """override if needed - need to filtered elements"""
        return self.model.get_all_by(user=current_user)

    def default_get_all(self):
        """override if needed - get all default elements"""
        return self.model.get_all_by(user=current_user)

    @login_required
    def create_retrieve(self):
        if request.method == 'GET':
            return self.retrieve()
        elif request.method == 'POST':
            return self.create()

    @login_required
    def retrieve(self):
        if request.args:
            instances = self.handle_get_filters(request.args)
        else:
            instances = self.default_get_all()
        return jsonify([instance.serialize_flat() for instance in instances])

    @login_required
    def create(self):
        include = self._get_form_include()
        form_data = serialize_request_form(include)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return jsonify({"Error": form_instance.errors}), 400
        form_data = self.before_create(form_data)
        rv = self.add_create(form_data)
        return jsonify(rv)

    @login_required
    def put(self, instance_id):
        """Used to update the whole element"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error}), 404
        include = self._get_form_include()
        form_data = serialize_request_form(include)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return jsonify({"Error": form_instance.errors}), 400
        rv = self.add_update(instance, form_data)
        return jsonify(rv)

    @login_required
    def patch(self, instance_id):
        """Used to (usually) update just a single item"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error}), 404
        updated_elements = self.handle_patch(instance, request.json)
        rv = self.add_update(instance, updated_elements)
        return jsonify(rv)

    @login_required
    def patch_put(self, instance_id):
        if request.method == 'PATCH':
            return self.patch(instance_id)
        elif request.method == 'PUT':
            return self.put(instance_id)

    @login_required
    def delete(self, instance_id):
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error}), 404
        rv = self.add_delete(instance)
        return jsonify(rv)

    def _register(self):
        """Override this to register more views"""

    def register(self):
        from webapp.client import client
        http_methods = ['GET']
        if self.creatable:
            http_methods += ['POST']
        client.add_url_rule('/{name_plural}'.format(name_plural=self.name_plural),
                            endpoint='{name}_retrieve_create'.format(name=self.name),
                            view_func=self.create_retrieve,
                            methods=http_methods)
        if self.editable:
            client.add_url_rule('/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                                endpoint='{name}_patch'.format(name=self.name),
                                view_func=self.patch_put,
                                methods=['PATCH', 'PUT'])
        if self.deletable:
            client.add_url_rule('/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                                endpoint='{name}_delete'.format(name=self.name),
                                view_func=self.delete,
                                methods=['DELETE'])
        self._register()


def unix_ts(dt):
    if not dt:
        return 0
    utc_naive = dt.replace(tzinfo=None) - dt.utcoffset()
    return (utc_naive - datetime.datetime(1970, 1, 1)).total_seconds()


def get_latest(model_name):
    from store.models import LatestCUD
    created, updated, deleted = 0, 0, 0
    latest_cud = LatestCUD.get_one_by(user=current_user, model_name=model_name)
    if latest_cud:
        created = unix_ts(latest_cud.created_ts)
        updated = unix_ts(latest_cud.updated_ts)
        deleted = unix_ts(latest_cud.deleted_ts)
    latest = max(created, updated, deleted)
    return {
        'model_name': model_name,
        'latest': latest,
        'created': created,
        'updated': updated,
        'deleted': deleted,
    }


def format_seconds_to_human(seconds):
    if not seconds:
        return "0 minutes"
    hours = seconds / 3600.0
    minutes = seconds / 60
    minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
    if hours >= 1.0:
        hours_repr = '%d %s' % (int(hours), "hour" if int(hours) == 1 else "hours")
        minutes = (seconds - int(hours) * 3600) / 60.0
        minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
        if minutes > 0.0:
            return '%s and %s' % (hours_repr, minutes_repr)
        return '%s' % hours_repr
    return minutes_repr


_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')


def create_jinja_helpers(app):
    @app.template_filter('seconds_to_human')
    def seconds_to_human(seconds):
        return format_seconds_to_human(seconds)

    @app.template_filter('format_dt')
    def format_datetime(dt, formatting="%A, %d %b %Y"):
        """
        Formats the datetime string provided in value into whatever format you want that is supported by python strftime
        http://strftime.org/
        :param formatting The specific format of the datetime
        :param dt a datetime object
        :return:
        """
        return dt.strftime(formatting)

    @app.template_filter('nl2br')
    def nl2br(text):
        text = unicode(escape(text))
        result = u'<br>'.join(u'%s' % p.replace('\n', '<br>\n') for p in _paragraph_re.split(text))
        return Markup(result)


def get_now_utc(tzspecific=True):
    """
    Helper function to avoid all confusion about datetime.utcnow
    :param tzspecific: should we return Timezone specific datetime (usually yes)
    :return:
    """
    if tzspecific:
        return datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    return datetime.datetime.utcnow()


def get_user_tz(user):
    return tz.tzoffset(None, datetime.timedelta(seconds=user.tz_offset_seconds))


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def get_today_user(user):
    """
    Gets the user specific beginning of today based on his timezone
    """
    now = get_now_user(user)
    return datetime.datetime(year=now.year, month=now.month, day=now.day, tzinfo=now.tzinfo)


def get_tomorrow_user(user):
    """
    Gets the user specific beginning of tomorrow based on his timezone
    """
    today = get_today_user(user)
    return today + datetime.timedelta(days=1)


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
