from flask_security import current_user
from flask_security.forms import LoginForm, get_message, verify_and_update_password, requires_confirmation, \
    RegisterForm, NewPasswordFormMixin, PasswordConfirmFormMixin, EmailFormMixin, Form
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

from store.models import User
from webapp import env_assets


class ExtendedRegisterForm(RegisterForm):
    name = StringField('Your Name', [DataRequired()])

    def __init__(self, *args, **kwargs):
        super(ExtendedRegisterForm, self).__init__(*args, **kwargs)
        if not self.next.data:
            from flask import request
            self.next.data = request.args.get('next', '')


class ExtendedLoginForm(LoginForm):
    def validate(self):
        if not super(LoginForm, self).validate():
            return False

        if self.email.data.strip() == '':
            self.email.errors.append(get_message('EMAIL_NOT_PROVIDED')[0])
            return False

        if self.password.data.strip() == '':
            self.password.errors.append(get_message('PASSWORD_NOT_PROVIDED')[0])
            return False

        self.user = User.get_one_by(email=self.email.data)

        if self.user is None or not verify_and_update_password(self.password.data, self.user):
            self.email.errors.append(env_assets.ExceptionMessages.INCORRECT_USER_PASSWORD)
            return False
        if not self.user.password:
            self.password.errors.append(get_message('PASSWORD_NOT_SET')[0])
            return False
        if requires_confirmation(self.user):
            self.email.errors.append(get_message('CONFIRMATION_REQUIRED')[0])
            return False
        if not self.user.is_active:
            self.email.errors.append(get_message('DISABLED_ACCOUNT')[0])
            return False
        return True


class SetupUserForm(Form, NewPasswordFormMixin, PasswordConfirmFormMixin, EmailFormMixin):
    submit = SubmitField("Next")

    def validate(self):
        if not super(SetupUserForm, self).validate():
            return False

        self.user = User.get_one_by(email=self.email.data)

        if self.user and current_user.is_authenticated and self.user.id != current_user.id:
            self.email.errors.append(env_assets.ExceptionMessages.USER_ALREADY_EXISTS_IN_DB)
            return False

        return True
