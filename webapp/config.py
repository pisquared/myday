import sensitive


class Constants(object):
    MAX_FILE_SIZE = 5 * 1024 * 1024  # 5 MB
    ADMIN_ROLE = "admin"


class Config(object):
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = sensitive.SECRET_KEY

    CACHE_TYPE = 'simple'

    SQLALCHEMY_DATABASE_URI = 'postgresql://app_user:%s@localhost:5432/app' % sensitive.ADMIN_DB_PASSWORD

    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_PASSWORD_SALT = sensitive.SECURITY_PASSWORD_SALT
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_CONFIRMABLE = False
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = False
    SECURITY_CHANGEABLE = True
    SECURITY_LOGOUT_URL = '/log-out'


class ConfigDev(Config):
    MODE = 'development'
    DEBUG = True
    DEBUG_FE = False  # change this to enable/disable FE debugging - i.e. remove asset compilation
    DEBUG_TB_ENABLED = False  # change this to enable/disable debug toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False


class ConfigTest(Config):
    MODE = 'testing'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://app_user:%s@localhost:5432/app_test' % sensitive.ADMIN_DB_PASSWORD
    WTF_CSRF_ENABLED = False
    SERVER_NAME = 'localhost:5000'


class ConfigProd(Config):
    MODE = 'production'


config_factory = {
    'testing': ConfigTest,
    'development': ConfigDev,
    'production': ConfigProd
}
