import importlib

from flask import Flask
from flask_admin import Admin, AdminIndexView, expose
from flask_assets import Environment, Bundle
from flask_cache import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_security import Security, SQLAlchemyUserDatastore, login_required, roles_required, user_registered
from flask_socketio import SocketIO
from flask_sqlalchemy_cache import FromCache
from flask_uploads import patch_request_class
from flask_wtf.csrf import CSRFProtect

from store.database import db
from webapp.config import config_factory, Constants
from webapp.lib import get_now_user


class MyHomeView(AdminIndexView):
    @expose('/')
    @login_required
    @roles_required(Constants.ADMIN_ROLE)
    def index(self):
        from store import models

        users = models.User.query.order_by(models.User.id).all()
        return self.render('admin/index.html', users=users)


env_assets = Environment()
admin = Admin(template_mode='bootstrap3', index_view=MyHomeView())
cache = Cache()
debug_toolbar = DebugToolbarExtension()
csrf = CSRFProtect()
migrate = Migrate()
security = Security()
socketio = SocketIO()


def _init_admin(app):
    from store.database import db, AdminModelView
    from store.models import LatestCUD, User, Role, GeneralSettings, JournalEntry, Task, \
        Activity, ActivitySession, ActivityReport

    admin.init_app(app)
    admin.add_view(AdminModelView(LatestCUD, db.session))

    admin.add_view(AdminModelView(Role, db.session))
    admin.add_view(AdminModelView(User, db.session))

    admin.add_view(AdminModelView(GeneralSettings, db.session))

    admin.add_view(AdminModelView(JournalEntry, db.session))

    admin.add_view(AdminModelView(Task, db.session))

    admin.add_view(AdminModelView(Activity, db.session))
    admin.add_view(AdminModelView(ActivitySession, db.session))
    admin.add_view(AdminModelView(ActivityReport, db.session))


def _init_assets(app):
    from webapp.assets import REGISTERED_APPS

    env_assets.init_app(app)
    app.config['css_layout_elements'] = [
        'css/vendor/calendar.css',
        'css/vendor/materialize.css',
        'css/global.css',
    ]
    css_layout = Bundle(*app.config['css_layout_elements'],
                        filters='cssmin', output='gen/layout.min.css')
    env_assets.register('css_layout', css_layout)

    app.config['js_layout_elements'] = [
        'js/vendor/jquery.js',
        'js/vendor/underscore.js',
        'js/vendor/backbone.js',
        'js/vendor/backbone.localStorage.js',
        'js/vendor/materialize.js',
        'js/vendor/moment.js',
        'js/vendor/calendar.js',
        'js/vendor/socket.io.js',
        'js/main.js',
        'js/calendar.js',
    ]
    js_layout = Bundle(*app.config['js_layout_elements'],
                       filters='jsmin', output='gen/layout.min.js')
    env_assets.register('js_layout', js_layout)

    # register apps for present
    js_present_included = [
        'js/framework.js',
    ]
    for appName in REGISTERED_APPS:
        js_present_included.append("js/models/{}.js".format(appName))
        js_present_included.append("app_{}/app.js".format(appName))

    app.config['js_present_elements'] = js_present_included
    js_present = Bundle(*app.config['js_present_elements'],
                        filters='jsmin', output='gen/present.min.js')
    env_assets.register('js_present', js_present)


def _init_security(app, db):
    from store import database
    from store.models import User, Role, GeneralSettings, LatestCUD
    from auth.forms import ExtendedRegisterForm, ExtendedLoginForm

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, user_datastore, register_form=ExtendedRegisterForm,
                      login_form=ExtendedLoginForm,
                      confirm_register_form=ExtendedRegisterForm)

    @user_registered.connect_via(app)
    def post_register(sender, **kwargs):
        from webapp.assets import DEFAULT_PANELS, REGISTERED_APPS

        user = kwargs.get('user')

        general_settings = GeneralSettings.create(user=user,
                                                  panels=DEFAULT_PANELS)
        database.add(general_settings)
        now = get_now_user(user)
        for model_name in REGISTERED_APPS:
            latest_cud = LatestCUD.create(user=user,
                                          model_name=model_name,
                                          created_ts=now)
            database.add(latest_cud)
        database.push()


def _init_cached_login_mgr(app):
    from store.models import User

    @app.login_manager.user_loader
    def _load_user(id=None):
        return User.query.options(FromCache(cache)).filter_by(id=id).first()


def _init_blueprints(app):
    from webapp.assets import REGISTERED_APPS
    from webapp.client import client
    app.register_blueprint(client)

    for model_name in REGISTERED_APPS:
        bp_module = importlib.import_module("webapp.apps.{}".format(model_name))
        bp = getattr(bp_module, model_name)
        app.register_blueprint(bp)


def create_app():
    return Flask(__name__)


def init_app(app, option, **kwargs):
    config = config_factory.get(option)
    app.config.from_object(config)
    updated_config = {k.upper(): v for k, v in kwargs.iteritems()}
    app.config.update(updated_config)

    # init extensions
    _init_admin(app)
    _init_assets(app)
    db.init_app(app)
    debug_toolbar.init_app(app)
    migrate.init_app(app, db, directory='store/migrations')
    _init_security(app, db)
    csrf.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    socketio.init_app(app)

    if option == 'production':
        cache.init_app(app)
        _init_cached_login_mgr(app)

    # patch max request size
    patch_request_class(app, Constants.MAX_FILE_SIZE)

    _init_blueprints(app)

    from webapp.lib import create_jinja_helpers
    create_jinja_helpers(app)

    return app


def verify_required_model_instances(app):
    from store.models import Role
    from store import database
    from sqlalchemy.exc import ProgrammingError
    with app.app_context():
        try:
            Role.get_or_create(name=Constants.ADMIN_ROLE)
            database.push()
        except ProgrammingError:
            pass
