"""
All strings or other assets that the application uses.
"""


class ExceptionMessages(object):
    UNKNOWN_ERROR = "Unknown error occurred."
    MISSING_PARAM = "Param {param} is missing."
    INSTANCE_NOT_EXISTS = '{instance} with id {id} does not exist'
    INVALID_JSON = "Invalid json in body of request."

    INSTANCE_ALREADY_EXISTS = '{instance} already exists.'

    SUCCESS_CREATING_OBJECT = "Successfully created {object_name}."

    FILE_TOO_LARGE = "File too large."

    NOT_YOUR_INSTANCE = "Not your {instance}"

    INCORRECT_USER_PASSWORD = "Incorrect username or password"

    ERRORS_ON_FORM = "There are some errors in your form"
    NOT_A_VALID_EMAIL = "Invalid email"
    USER_ALREADY_EXISTS_IN_DB = "Sorry, but user with this email already exists in our database. You need to pick a different email"


REGISTERED_APPS = ['task', 'journal_entry', 'activity', 'activity_report', ]

from webapp.apps.task.manifest import NOT_STARTED_TASKS, RUNNING_TASKS, DONE_TASKS
from webapp.apps.journal_entry.manifest import JOURNAL_ENTRIES, JOURNAL_ENTRY_CREATE
from webapp.apps.activity.manifest import RUNNING_ACTIVITIES, NOT_STARTED_ACTIVITIES
from webapp.apps.activity_report.manifest import ACTIVITY_REPORT

DEFAULT_PANELS = [
    {
        'name': 'To do',
        'card_components': [
            NOT_STARTED_TASKS,
            NOT_STARTED_ACTIVITIES,
        ]
    },
    {
        'name': 'Now',
        'card_components': [
            JOURNAL_ENTRY_CREATE,
            RUNNING_TASKS,
            RUNNING_ACTIVITIES,
        ]
    },
    {
        'name': 'Done',
        'card_components': [
            ACTIVITY_REPORT,
            DONE_TASKS,
            JOURNAL_ENTRIES,
        ]
    },
]
