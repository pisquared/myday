var ActivityReportConstants = {
  APP_NAME: "activity_report"
};

var ActivityReportModel = AppModel.extend({
  urlRoot: '/activity_reports',

  defaults: {
    'id': null,
    'name': null,
    'duration': null
  }
});