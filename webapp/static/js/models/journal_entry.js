var JournalEntryConstants = {
  APP_NAME: "journal_entry"
};

var JournalEntryModel = AppModel.extend({
  urlRoot: '/journal_entries',
  defaults: {
    'id': null,
    'body': null,
    'created_ts': ''
  }
});

var JournalEntryCollection = AppCollection.extend({
  appName: JournalEntryConstants.APP_NAME,
  url: '/journal_entries',
  model: JournalEntryModel,

  comparator: function (model) {
    return -moment(model.get("created_ts")).unix();
  }
});
