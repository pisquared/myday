var ActivityConstants = {
  APP_NAME: "activity"
};

var ActivityModel = AppModel.extend({
  urlRoot: '/activities',

  defaults: {
    'id': null,
    'name': null,
    'icon': null,
    'is_running': false
  },

  toggleRun: function () {
    var newStatus = !(this.get("is_running"));
    this.saveAndPush({is_running: newStatus}, {patch: true})
  }
});

var ActivityCollection = AppCollection.extend({
  appName: ActivityConstants.APP_NAME,
  url: '/activities',
  model: ActivityModel,

  comparator: 'name'
});