var TaskConstants = {
  APP_NAME: "task",
  STATUS_DONE: "DONE",
  STATUS_NOT_STARTED: "NOT_STARTED",
  STATUS_IN_PROGRESS: "IN_PROGRESS"
};

var TaskModel = AppModel.extend({
  urlRoot: '/tasks',

  defaults: {
    'id': null,
    'body': null,
    'status': false,
    'start_ts': null,
    'due_ts': null,
    'done_ts': null
  },

  is_done: function () {
    return this.attributes.status === TaskConstants.STATUS_DONE;
  },

  is_running: function () {
    return this.attributes.status === TaskConstants.STATUS_IN_PROGRESS;
  },

  toggleRun: function () {
    var newStatus = this.is_running() ? TaskConstants.STATUS_NOT_STARTED : TaskConstants.STATUS_IN_PROGRESS;
    this.saveAndPush({status: newStatus}, {patch: true});
  },

  toggleDone: function () {
    var newStatus = this.is_done() ? TaskConstants.STATUS_NOT_STARTED : TaskConstants.STATUS_DONE;
    if (this.is_done())
      this.saveAndPush({status: newStatus}, {patch: true});
    else
      this.saveAndPush({status: newStatus, done_ts: new Date()}, {patch: true});
  }
});

var TaskCollection = AppCollection.extend({
  appName: TaskConstants.APP_NAME,
  url: '/tasks',
  model: TaskModel,

  comparator: function (model) {
    return -moment(model.get("created_ts")).unix();
  }
});