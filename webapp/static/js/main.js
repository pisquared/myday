var DATETIME_FORMAT = 'YYYY-MM-DD HH:mm';

$('#hamburger').sideNav({
  menuWidth: 400, // Default is 300
  edge: 'left', // Choose the horizontal origin
  draggable: true // Choose whether you can drag to open on touch screens
});

var CSRF_TOKEN = $('meta[name=csrf-token]').attr('content');

$.ajaxSetup({
  beforeSend: function (xhr, settings) {
    if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", CSRF_TOKEN);
    }
  }
});

var oldSync = Backbone.ajaxSync;
Backbone.ajaxSync = function (method, model, options) {
  options.beforeSend = function (xhr) {
    xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
  };
  return oldSync(method, model, options);
};

// update csrf
function updateCSRF() {
  $.ajax({
    url: '/csrf-token'
  }).done(function (csrftoken) {
    $('meta[name=csrf-token]').attr('content', CSRF_TOKEN);
    $('.csrf-token').each(function () {
      $(this).val(csrftoken);
    });
  })
}

setInterval(function () {
  updateCSRF()
}, 10 * 60 * 60 * 1000);

$(document).ready(function () {
  setTimeout(function () {
    $('#journal-entry-textarea').focus();
  }, 0);
  $('.modal').modal();
});