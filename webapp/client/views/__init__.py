__all__ = [
    # main.py
    'main',
]

# deprecated to keep older scripts who import this from breaking
from main import *
