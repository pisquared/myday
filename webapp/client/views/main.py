import datetime
from flask import render_template, request, redirect, url_for, jsonify
from flask_security import current_user, login_required, logout_user
from flask_socketio import emit, join_room
from flask_wtf.csrf import generate_csrf

from store import database
from store.models import GeneralSettings, Task
from webapp import socketio, get_now_user
from webapp.assets import REGISTERED_APPS
from webapp.client import client
from webapp.lib import get_latest, get_user_tz, get_today_user


@login_required
@client.route("/logout")
def logout():
    logout_user()
    return render_template("logout.html")


@client.route("/")
def index():
    if not current_user.is_authenticated:
        return render_template("landing.html")
    return redirect(url_for('client.present'))


@client.route("/present")
@login_required
def present():
    panels = GeneralSettings.get_one_by(user=current_user).panels
    day = get_today_user(current_user)
    return render_template("present.html",
                           title="DayLog",
                           panels=panels,
                           day=day)


@client.route("/api/events")
@login_required
def api_events():
    start_unix_ts = int(request.args.get('from'))
    end_unix_ts = int(request.args.get('to'))
    start_ts = datetime.datetime.fromtimestamp(start_unix_ts / 1e3).replace(tzinfo=get_user_tz(current_user))
    end_ts = datetime.datetime.fromtimestamp(end_unix_ts / 1e3).replace(tzinfo=get_user_tz(current_user))
    done_tasks = Task.query.filter(Task.user == current_user,
                                   Task.deleted == False,
                                   Task.status == Task._STATUS_DONE,
                                   Task.done_ts >= start_ts,
                                   Task.done_ts <= end_ts)
    due_tasks = Task.query.filter(Task.user == current_user,
                                  Task.deleted == False,
                                  Task.status == Task._STATUS_DONE,
                                  Task.due_ts >= start_ts,
                                  Task.due_ts <= end_ts)
    rv = []
    for task_collection, cls in [(due_tasks, 'danger'), (done_tasks, 'success')]:
        for task in task_collection:
            rv.append({
                'id': task.id,
                'title': task.body,
                'class': "event-{}".format(cls),
                'start': int(task.done_ts.strftime("%s")) * 1000,
                'end': int(task.done_ts.strftime("%s")) * 1000
            })
    return jsonify({
        "success": 1,
        "result": rv
    })


@client.route('/csrf-token')
def get_csrf_token():
    return generate_csrf()


@socketio.on('SYN')
def socket_connect(json):
    payload = {}
    if current_user.is_authenticated:
        tz_offset_minutes = json.get('tz_offset_minutes')
        tz_offset_seconds = -(tz_offset_minutes * 60)
        if current_user.tz_offset_seconds != tz_offset_seconds:
            current_user.tz_offset_seconds = tz_offset_seconds
            database.add(current_user)
            database.push()
        model_updates = {}
        for model_name in REGISTERED_APPS:
            model_updates[model_name] = get_latest(model_name)
        join_room(current_user.id)
        payload = {'user': {'id': current_user.id,
                            'name': current_user.name},
                   'request_sid': request.sid,
                   'datetime': str(get_now_user(current_user)),
                   'models_updates': model_updates}
    print('< SYN: ' + str(json))
    print('> ACK: ' + str(payload))
    emit('ACK', payload)
