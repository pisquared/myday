var JournalEntryEditView = AppEditView.extend({
  appName: JournalEntryConstants.APP_NAME,

  template: _.template($("#fe-template-journal-entry-edit").html()),

  getEditAttributes: function () {
    return {
      body: this.$('#journal-entry-body').val()
    }
  }
});


var JournalEntryView = AppElementView.extend({
  EditViewClass: JournalEntryEditView,
  template: _.template($("#fe-template-journal-entry").html()),

  formatDate: function (date) {
    return moment(date).format('HH:mm');
  },

  render: function () {
    var jsonModel = this.model.toJSON();
    var that = this;
    $.each(['created_ts'], function (_, key) {
      jsonModel[key] = that.formatDate(jsonModel[key]);
    });
    var html = this.template(jsonModel);
    this.setElement(html);
    return this;
  }
});

var JournalEntryListView = AppListView.extend({
  appName: JournalEntryConstants.APP_NAME,

  render: function () {
    this.$('.cardc-content').html("");
    var that = this;
    var collection = this.collection.models;
    if (collection.length)
      this.$el.show();
    else
      this.$el.hide();
    $.each(collection, function (_, model) {
      var item = new JournalEntryView({model: model});
      that.$('.cardc-content').append(item.$el);
    });
  }
});

var JournalEntryCreateView = Backbone.View.extend({
  el: '#journal_entry_create',

  events: {
    "click button": "create"
  },

  create: function () {
    var attrs = {
      body: $('textarea').val(),
      created_ts: moment(new Date()).format(),
      request_sid: REQUEST_SID
    };
    var model = new JournalEntryModel();
    this.collection.createModel(model, attrs, {at: 0});
    $('textarea').val('');
  }
});

var journalEntryCollection = new JournalEntryCollection();

var journalEntries = new JournalEntryListView({
  el: '#journal_entries',
  collection: journalEntryCollection
});

new JournalEntryCreateView({collection: journalEntryCollection});

ApplicationRegistry.register(JournalEntryConstants.APP_NAME, {
  'collection': journalEntryCollection,
  'views': [journalEntries]
});
