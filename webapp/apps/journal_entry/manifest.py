JOURNAL_ENTRY_CREATE = {
    'name': 'journal_entry_create',
    'template': 'journal_entry/create.html',
}


JOURNAL_ENTRIES = {
    'name': 'journal_entries',
    'fe_templates': {
        'journal-entry': 'journal_entry/single.html',
        'journal-entry-edit': 'journal_entry/edit.html',
    },
}

app = {
    'card_components': [JOURNAL_ENTRY_CREATE, JOURNAL_ENTRIES, ]
}
