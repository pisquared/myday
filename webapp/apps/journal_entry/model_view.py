import datetime
from flask_login import current_user
from wtforms_alchemy import ModelForm

from store.models import JournalEntry
from webapp import get_now_user
from webapp.apps.task.model_view import task_vm
from webapp.lib import ModelView, human_parse_dt, parse_dt, get_today_user, get_tomorrow_user


class JournalEntryForm(ModelForm):
    class Meta:
        model = JournalEntry
        include = ['body', ]


def get_now_handler(user, **kwargs):
    return get_now_user(user)


def datetime_human_parser(value, **kwargs):
    try:
        value = human_parse_dt(value)
    except:
        pass
    return value


def get_timedelta_from_time(ctx, **kwargs):
    return ctx.get('start_ts') + datetime.timedelta(hours=1)


class JournalEntryModelView(ModelView):
    CREATABLE_ENTITIES = {
        'task': {
            'name': 'task',
            'vm': task_vm,
            'trigger_words': ['todo', 'task', ],
            'map_main': 'body',
            'fields': {
                'start': {
                    'map': 'start_ts',
                    'handler': datetime_human_parser,
                    'default_handler': get_now_handler,
                },
                'due': {
                    'map': 'due_ts',
                    'handler': datetime_human_parser,
                },
            }
        }
    }

    def _create_entity_from_journal_entry(self, entity, trigger_word, lower_body):
        vm = entity.get('vm')
        fields = entity.get('fields')

        ctx = {}
        main = entity.get('map_main')
        line_split = lower_body.split('\n')
        ctx[main] = ''.join(line_split[0].split('{}:'.format(trigger_word))[1:]).strip()
        for line in line_split[1:]:
            for field_name, field in fields.iteritems():
                if '{}:'.format(field_name) in line:
                    value = ''.join(line.split('{}:'.format(field_name))[1:]).strip()
                    mapped = field.get('map')
                    handler = field.get('handler')
                    ctx[mapped] = handler(user=current_user, value=value, ctx=ctx)
        for field in fields.itervalues():
            mapped = field.get('map')
            default_handler = field.get('default_handler')
            if default_handler and mapped not in ctx:
                default = field.get('default')
                ctx[mapped] = default_handler(user=current_user, value=default, ctx=ctx)
        creation = vm.add_create(ctx)
        rv = dict()
        rv['{}_id'.format(entity['name'])] = creation['instance']['id']
        return rv

    def _parse_journal_entry_body(self, body):
        lower_body = body.lower()
        for entity_name, entity in self.CREATABLE_ENTITIES.iteritems():
            trigger_words = entity.get('trigger_words')
            for trigger_word in trigger_words:
                if lower_body.startswith('{}:'.format(trigger_word)):
                    return self._create_entity_from_journal_entry(entity, trigger_word, lower_body)

    def _get_between(self, start_ts, end_ts, **kwargs):
        return JournalEntry.query.filter(JournalEntry.user_id == current_user.id,
                                         JournalEntry.deleted == False,
                                         JournalEntry.created_ts >= start_ts,
                                         JournalEntry.created_ts <= end_ts).all()

    def before_create(self, form_data):
        body = form_data.get('body')
        update = self._parse_journal_entry_body(body)
        if update:
            form_data.update(update)
        return form_data

    def handle_get_filters(self, filters):
        now = get_now_user(current_user)
        start_ts = parse_dt(filters.get('start_ts')) or get_today_user(current_user)
        end_ts = parse_dt(filters.get('end_ts')) or get_tomorrow_user(current_user)
        return self._get_between(start_ts=start_ts, end_ts=end_ts, now=now)

    def default_get_all(self):
        now = get_now_user(current_user)
        start_ts = get_today_user(current_user)
        end_ts = get_tomorrow_user(current_user)
        return self._get_between(start_ts=start_ts, end_ts=end_ts, now=now)


journal_entry_vm = JournalEntryModelView(name='journal_entry',
                                         model=JournalEntry,
                                         form=JournalEntryForm)

journal_entry_vm.register()