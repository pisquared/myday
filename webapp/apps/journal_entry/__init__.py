from flask import Blueprint

journal_entry = Blueprint('app_journal_entry', __name__,
                          static_folder='static',
                          static_url_path='/static/app_journal_entry',
                          template_folder='templates')
from . import model_view
