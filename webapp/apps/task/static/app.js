var TaskEditView = AppEditView.extend({
  appName: TaskConstants.APP_NAME,

  template: _.template($("#fe-template-task-edit").html()),

  getEditAttributes: function () {
    return {
      body: this.$('#task-body').val(),
      start_ts: this.$('#task-start-ts').val(),
      due_ts: this.$('#task-due-ts').val()
    }
  }
});

var TaskView = AppElementView.extend({
  EditViewClass: TaskEditView,
  template: _.template($("#fe-template-task").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      'click .task-checkbox': 'clickToggleDone',
      'click .task-start': 'clickRun'
    });
  },

  clickToggleDone: function (event) {
    this.model.toggleDone();
  },

  clickRun: function (event) {
    this.model.toggleRun();
  }
});

var TaskListView = AppListView.extend({
  appName: TaskConstants.APP_NAME,

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.status = options.status;
    this.done_ts = options.done_ts;
    this.standalone = options.standalone || false;
  },

  render: function () {
    var elm = this.standalone ? this.$('.cardc-content') : this.$el;
    elm.html("");
    var collection = this.collection.where({status: this.status});
    if (this.done_ts) {
      collection = _.filter(collection, function (el) {
        var doneTs = el.get('done_ts');
        if (!doneTs)
          return false;
        return moment(doneTs).format("YYYY-MM-DD") === this.done_ts;
      }.bind(this));
    }

    if (this.standalone) {
      if (collection.length)
        this.$el.show();
      else
        this.$el.hide();
    }
    $.each(collection, function (_, model) {
      var item = new TaskView({model: model});
      elm.append(item.$el);
    });
  }
});

var TaskCreateView = Backbone.View.extend({
  el: '#modal',
  template: _.template($("#fe-template-task-create").html()),

  events: {
    'click .btn-submit': 'onCreate'
  },

  render: function () {
    var html = this.template();
    this.$('.modal-content').html(html);
    // need to redelegate events as it's a shared modal for both create and edit
    this.$el.off();
    this.delegateEvents();
    return this;
  },

  onCreate: function () {
    var attrs = {
      body: this.$('#task-body').val(),
      status: TaskConstants.STATUS_NOT_STARTED,
      start_ts: this.$('#task-start-ts').val(),
      due_ts: this.$('#task-due-ts').val(),
      request_sid: REQUEST_SID
    };
    var model = new TaskModel();
    this.collection.createModel(model, attrs, {at: 0});
  }
});

var TaskContainerView = Backbone.View.extend({
  template: _.template($("#fe-template-task-container").html()),

  events: {
    'click .action-create': 'clickCreate'
  },

  createView: null,

  initialize: function () {
    this.render();
  },

  clickCreate: function () {
    if (!this.createView)
      this.createView = new TaskCreateView({collection: this.collection});
    this.createView.render();
  },

  render: function () {
    var html = this.template();
    this.$el.show();
    var that = this;
    this.$('.cardc-content').html(html);
    var taskListEl = this.$('.task-list');
    this.taskListView = new TaskListView({
      el: taskListEl,
      status: TaskConstants.STATUS_NOT_STARTED,
      collection: that.collection
    });
    return this;
  }
});

var taskCollection = new TaskCollection();

var notStartedTasks = new TaskContainerView({
  el: '#not_started_tasks',
  collection: taskCollection
});

var runningTasks = new TaskListView({
  el: '#running_tasks',
  collection: taskCollection,
  status: TaskConstants.STATUS_IN_PROGRESS,
  standalone: true
});

var doneTasks = new TaskListView({
  el: '#done_tasks',
  collection: taskCollection,
  status: TaskConstants.STATUS_DONE,
  done_ts: moment(new Date()).format('YYYY-MM-DD'),
  standalone: true,
  updatePeriod: function (start, end) {
    this.done_ts = start;
  }
});

ApplicationRegistry.register(TaskConstants.APP_NAME, {
  'collection': taskCollection,
  'views': [notStartedTasks, runningTasks, doneTasks]
});
