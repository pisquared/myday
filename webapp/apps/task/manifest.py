NOT_STARTED_TASKS = {
    'name': 'not_started_tasks',
    'fe_templates': {
        'task': 'task/single.html',
        'task-container': 'task/container.html',
        'task-edit': 'task/edit.html',
        'task-create': 'task/create.html',
    },
}

RUNNING_TASKS = {
    'name': 'running_tasks',
}

DONE_TASKS = {
    'name': 'done_tasks',
}

app = {
    'card_components': [NOT_STARTED_TASKS, RUNNING_TASKS, DONE_TASKS, ]
}
