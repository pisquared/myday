from flask import Blueprint

task = Blueprint('app_task', __name__,
                 static_folder='static',
                 static_url_path='/static/app_task',
                 template_folder='templates')
from . import model_view
