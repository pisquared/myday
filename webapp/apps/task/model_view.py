import datetime

from flask_login import current_user
from wtforms_alchemy import ModelForm

from store.models import Task
from webapp import get_now_user
from webapp.lib import ModelView, parse_dt


class TaskForm(ModelForm):
    class Meta:
        model = Task
        include = ['body', 'start_ts', 'due_ts', ]


class TaskModelView(ModelView):
    def before_create(self, form_data):
        now = get_now_user(current_user)
        start_ts = form_data.get('start_ts')
        if not start_ts:
            form_data['start_ts'] = now
        return form_data

    def _sort_and_format(self, tasks):
        return sorted(tasks, key=lambda t: t.created_ts, reverse=True)

    def _get_start_ts_end_of_day(self, start_ts=None, end_ts=None):
        start_ts = start_ts or get_now_user(current_user)
        start_of_the_day = datetime.datetime(year=start_ts.year, month=start_ts.month, day=start_ts.day)
        return start_of_the_day.replace(hour=23, minute=59, second=59)

    def retrieve_not_started(self, start_ts=None, end_ts=None):
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts)
        tasks = self.model.query.filter(Task.user_id == current_user.id,
                                        Task.deleted == False,
                                        Task.status == Task._STATUS_NOT_STARTED,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def retrieve_done(self, start_ts=None, end_ts=None):
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts)
        tasks = self.model.query.filter(Task.user_id == current_user.id,
                                        Task.deleted == False,
                                        Task.status == Task._STATUS_DONE,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def retrieve_all(self, start_ts=None, end_ts=None):
        _start_ts_end_of_day = self._get_start_ts_end_of_day(start_ts=start_ts)
        tasks = self.model.query.filter(Task.user_id == current_user.id,
                                        Task.deleted == False,
                                        Task.start_ts <= _start_ts_end_of_day).all()
        return self._sort_and_format(tasks)

    def handle_get_filters(self, filters):
        retrieve_filters = dict()
        if 'start_ts' in filters:
            retrieve_filters['start_ts'] = parse_dt(filters['start_ts'])
        if 'end_ts' in filters:
            retrieve_filters['end_ts'] = parse_dt(filters['end_ts'])
        if 'is_done' in filters:
            if filters['is_done'] == '1':
                return self.retrieve_done(**retrieve_filters)
            elif filters['is_done'] == '0':
                return self.retrieve_not_started(**retrieve_filters)
        return self.retrieve_all(**retrieve_filters)

    def handle_patch(self, instance, update):
        rv = super(TaskModelView, self).handle_patch(instance, update)
        now = get_now_user(current_user)
        if 'status' in update:
            status = update.pop('status')
            if status in [Task._STATUS_NOT_STARTED, Task._STATUS_IN_PROGRESS, Task._STATUS_DONE, ]:
                rv['status'] = status
                if status == Task._STATUS_DONE:
                    rv['done_ts'] = now
                elif status == Task._STATUS_IN_PROGRESS:
                    rv['in_progress_ts'] = now
        return rv


task_vm = TaskModelView(name='task',
                        model=Task,
                        form=TaskForm,
                        extra_actions=True)

task_vm.register()
