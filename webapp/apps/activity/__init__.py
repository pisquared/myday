from flask import Blueprint

activity = Blueprint('app_activity', __name__,
                     static_folder='static',
                     static_url_path='/static/app_activity',
                     template_folder='templates')
from . import model_view
