from wtforms_alchemy import ModelForm

from store.models import Activity
from webapp.lib import ModelView


class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        include = ['name', ]


class ActivityModelView(ModelView):
    def handle_patch(self, instance, update):
        rv = super(ActivityModelView, self).handle_patch(instance, update)
        if 'is_running' in update:
            is_running = update.pop('is_running')
            if is_running in [True, False]:
                rv['is_running'] = is_running
                if is_running:
                    instance.start()
                else:
                    instance.stop()
        return rv


activity_vm = ActivityModelView(name='activity',
                                model=Activity)

activity_vm.register()
