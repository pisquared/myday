var ActivityView = AppElementView.extend({
  template: _.template($("#fe-template-activity").html()),

  events: function () {
    var parentEvents = AppElementView.prototype.events(this);
    return _.extend(parentEvents, {
      'click .activity-toggle': 'clickRun'
    });
  },

  clickRun: function (event) {
    this.model.toggleRun();
  }
});

var ActivityListView = AppListView.extend({
  appName: ActivityConstants.APP_NAME,

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.is_running = options.is_running;
  },

  render: function () {
    this.$('.cardc-content').html("");
    var that = this;
    var collection = this.collection.where({is_running: this.is_running});
    if (collection.length)
      this.$el.show();
    else
      this.$el.hide();
    $.each(collection, function (_, model) {
      var item = new ActivityView({model: model});
      that.$('.cardc-content').append(item.$el);
    });
  }
});

var activityCollection = new ActivityCollection();

var activityNotRunningView = new ActivityListView({
  el: '#not_started_activities',
  collection: activityCollection,
  is_running: false
});

var activityRunningView = new ActivityListView({
  el: '#running_activities',
  collection: activityCollection,
  is_running: true
});

ApplicationRegistry.register(ActivityConstants.APP_NAME, {
  'collection': activityCollection,
  'views': [activityNotRunningView, activityRunningView]
});