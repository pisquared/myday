NOT_STARTED_ACTIVITIES = {
    'name': 'not_started_activities',
    'fe_templates': {
        'activity': 'activity/single.html',
        'activity-container': 'activity/container.html',
        'activity-edit': 'activity/edit.html',
        'activity-create': 'activity/create.html',
    },
}

RUNNING_ACTIVITIES = {
    'name': 'running_activities',
}

app = {
    'card_components': [NOT_STARTED_ACTIVITIES, RUNNING_ACTIVITIES, ]
}
