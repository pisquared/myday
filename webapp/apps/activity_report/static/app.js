var ActivityReportCollection = AppCollection.extend({
  appName: ActivityReportConstants.APP_NAME,
  url: '/activity_reports',
  model: ActivityReportModel,

  comparator: 'name'
});


var ActivityReportView = AppElementView.extend({
  template: _.template($("#fe-template-activity-report").html())
});

var ActivityReportListView = AppListView.extend({
  appName: ActivityConstants.APP_NAME,

  initialize: function (options) {
    AppListView.prototype.initialize.apply(this, options);
    this.is_running = options.is_running;
  },

  render: function () {
    this.$('.cardc-content').html("");
    var that = this;
    var collection = this.collection.models;
    if (collection.length)
      this.$el.show();
    else
      this.$el.hide();
    $.each(collection, function (_, model) {
      var item = new ActivityReportView({model: model});
      that.$('.cardc-content').append(item.$el);
    });
  }
});

var activityReportCollection = new ActivityReportCollection();

var activityReportListView = new ActivityReportListView({
  el: '#activity_reports',
  collection: activityReportCollection
});

ApplicationRegistry.register(ActivityReportConstants.APP_NAME, {
  'collection': activityReportCollection,
  'views': [activityReportListView]
});