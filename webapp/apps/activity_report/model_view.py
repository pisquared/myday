from store.models import ActivityReport
from webapp.lib import ModelView


# @login_required
# @activity.route('/activity_reports')
# def get_activity_report():
#     today = get_today_user(current_user)
#     activity_sessions = ActivitySession.query.filter(ActivitySession.user == current_user,
#                                                      ActivitySession.start_ts > today).all()
#     activity_sessions = [activity_session.serialize_flat() for activity_session in activity_sessions]
#     activity_durations = defaultdict(lambda: 0)
#     for activity_session in activity_sessions:
#         activity_name = activity_session['activity']['name']
#         activity_duration = activity_session['duration']
#         activity_durations[activity_name] += activity_duration
#     activity_reports = []
#     for name, duration in activity_durations.iteritems():
#         activity_reports.append({
#             'name': name,
#             'duration': duration
#         })
#     return jsonify(activity_reports)


activity_report_vm = ModelView(name='activity_report',
                               model=ActivityReport,
                               creatable=False, editable=False, deletable=False,
                               )

activity_report_vm.register()
