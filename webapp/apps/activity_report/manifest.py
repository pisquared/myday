ACTIVITY_REPORT = {
    'name': 'activity_reports',
    'fe_templates': {
        'activity-report': 'activity_report/single.html',
        'activity-report-container': 'activity_report/container.html',
    },
}

app = {
    'card_components': [ACTIVITY_REPORT, ]
}
