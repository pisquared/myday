from flask import Blueprint

activity_report = Blueprint('app_activity_report', __name__,
                            static_folder='static',
                            static_url_path='/static/app_activity_report',
                            template_folder='templates')
from . import model_view
