import json
from pprint import pprint
from time import sleep

import datetime
import re
from dateparser import parse as human_dateparse
from dateutil import tz
from dateutil.parser import parse as dateparse
from flask import Flask
from flask_login import UserMixin, current_user, login_user
from flask_security import Security, SQLAlchemyUserDatastore, RoleMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.collections import InstrumentedList

import sensitive


# HELPER METHODS
from webapp.lib import get_now_utc


def get_user_tz(user):
    return tz.tzoffset(None, datetime.timedelta(seconds=user.tz_offset_seconds))


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def parse_dt_strategy(value, parser_f):
    dt = None
    if value:
        try:
            dt = parser_f(value).replace(tzinfo=get_user_tz(current_user))
        except TypeError, ValueError:
            dt = None
    return dt


def human_parse_dt(value):
    return parse_dt_strategy(value, human_dateparse)


def parse_dt(value):
    return parse_dt_strategy(value, dateparse)


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


db = SQLAlchemy()


def create_db():
    db.create_all()


def drop_db():
    db.session.remove()
    db.drop_all()


def update(model, **kwargs):
    for k, w in kwargs.iteritems():
        setattr(model, k, w)
    db.session.add(model)
    return model


def add(instance):
    db.session.add(instance)


def delete(instance):
    now = get_now_utc()
    instance.deleted = True
    instance.deleted_ts = now
    db.session.add(instance)


def push():
    db.session.commit()


class ModelController(object):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance

    @classmethod
    def _filter_method(cls, filters=None, **kwargs):
        if not filters:
            filters = []
        if hasattr(cls, "deleted"):
            kwargs['deleted'] = kwargs.get('deleted', False)
        # noinspection PyUnresolvedReferences
        return cls.query.filter_by(**kwargs).filter(*filters)

    @classmethod
    def get_one(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_by(cls, first=0, last=None, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()[first:last]

    @classmethod
    def get_one_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_all_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.get_one_by(**kwargs)
        if not instance:
            instance = cls.create(**kwargs)
            add(instance)
        return instance

    def serialize_flat(self):
        """
        Serializes object to dict
        It will ignore fields that are not encodable (set them to 'None').

        It doesn't auto-expand relations (since this could lead to self-references, and loop forever).
        :return:
        """
        if isinstance(self.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(self) if not x.startswith('_') and x != 'metadata']:
                data = self.__getattribute__(field)
                try:
                    if isinstance(data, datetime.datetime):
                        data = str(data)
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    if isinstance(data, InstrumentedList):
                        continue  # pragma: no cover
                    fields[field] = data
                except TypeError:
                    pass  # Don't assign anything
            # a json-encodable dict
            return fields


class AwareDateTime(db.TypeDecorator):
    """
    Results returned as aware datetimes, not naive ones.
    """

    impl = db.DateTime

    def process_result_value(self, value, dialect):
        if current_user and current_user.is_authenticated and value:
            tzinfo = tz.tzoffset(None, datetime.timedelta(seconds=current_user.tz_offset_seconds))
            return value.astimezone(tzinfo)
        return value


class Deletable(object):
    """
    This interface adds deletable properties to a model.
    """
    deleted = db.Column(db.Boolean, default=False)
    deleted_ts = db.Column(AwareDateTime(timezone=True))

    def undelete(self):
        self.deleted = False
        self.deleted_ts = None
        db.session.add(self)


class Datable(object):
    created_ts = db.Column(AwareDateTime(timezone=True))
    updated_ts = db.Column(AwareDateTime(timezone=True))


roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(ModelController, db.Model, RoleMixin):
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '<Role %r>' % self.email


class User(ModelController, db.Model, UserMixin, Deletable):
    tz_offset_seconds = db.Column(db.Integer, default=0)

    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))


class JournalEntry(ModelController, db.Model, Datable, Deletable):
    body = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='journal_entries')


class Task(ModelController, db.Model, Datable, Deletable):
    body = db.Column(db.String)

    start_ts = db.Column(AwareDateTime(timezone=True))
    due_ts = db.Column(AwareDateTime(timezone=True))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='tasks')


class Event(ModelController, db.Model, Datable, Deletable):
    body = db.Column(db.String)

    start_ts = db.Column(AwareDateTime(timezone=True))
    due_ts = db.Column(AwareDateTime(timezone=True))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='events')


class Activity(ModelController, db.Model, Datable, Deletable):
    name = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='activities')


class ActivitySession(ModelController, db.Model, Datable, Deletable):
    start_ts = db.Column(AwareDateTime(timezone=True))
    end_ts = db.Column(AwareDateTime(timezone=True))

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref='sessions')

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='activity_sessions')


# CREATE INSTANCES
class App(object):
    def __init__(self):
        self.config = {}
        self.extensions = {}


app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'fdasfdasfasd'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://app_user:%s@localhost:5432/console' % sensitive.ADMIN_DB_PASSWORD
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

security = Security()
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security.init_app(app, user_datastore)

db.init_app(app)


def get_now_handler(user, **kwargs):
    return get_now_user(user)


def datetime_human_parser(value, **kwargs):
    try:
        value = human_parse_dt(value)
    except:
        pass
    return value


def get_timedelta_from_time(ctx, **kwargs):
    return ctx.get('start_ts') + datetime.timedelta(hours=1)


CREATABLE_ENTITIES = {
    'task': {
        'name': 'task',
        'model': Task,
        'trigger_words': ['todo', 'task', ],
        'map_main': 'body',
        'fields': {
            'start': {
                'map': 'start_ts',
                'handler': datetime_human_parser,
                'default_handler': get_now_handler,
            },
            'due': {
                'map': 'due_ts',
                'handler': datetime_human_parser,
            },
        }
    }
}


def _create_entity_from_journal_entry(entity, trigger_word, lower_body):
    model = entity.get('model')
    fields = entity.get('fields')

    ctx = {}
    main = entity.get('map_main')
    line_split = lower_body.split('\n')
    ctx[main] = ''.join(line_split[0].split('{}:'.format(trigger_word))[1:]).strip()
    for line in line_split[1:]:
        for field_name, field in fields.iteritems():
            if '{}:'.format(field_name) in line:
                value = ''.join(line.split('{}:'.format(field_name))[1:]).strip()
                mapped = field.get('map')
                handler = field.get('handler')
                ctx[mapped] = handler(user=current_user, value=value, ctx=ctx)
    for field in fields.itervalues():
        mapped = field.get('map')
        default_handler = field.get('default_handler')
        if default_handler and mapped not in ctx:
            default = field.get('default')
            ctx[mapped] = default_handler(user=current_user, value=default, ctx=ctx)
    now = get_now_user(current_user)
    instance = model.create(created_ts=now, user=current_user, **ctx)
    add(instance)
    push()


def _parse_journal_entry_body(body):
    lower_body = body.lower()
    for entity_name, entity in CREATABLE_ENTITIES.iteritems():
        trigger_words = entity.get('trigger_words')
        for trigger_word in trigger_words:
            if lower_body.startswith('{}:'.format(trigger_word)):
                return _create_entity_from_journal_entry(entity, trigger_word, lower_body)


def beep():
    print "\a"


def beep_n(n, sec=0.4):
    for _ in range(n):
        beep()
        sleep(sec)


def infinite_beep():
    while True:
        beep_n(5)
        sleep(1)


def parse_command(command):
    if command == 'tasks':
        pprint([task.serialize_flat() for task in user.tasks])
    else:
        _parse_journal_entry_body(command)


with app.app_context():
    create_db()

    user = User.create(tz_offset_seconds=3 * 60 * 60)

    with app.test_request_context('/'):
        login_user(user)

        task = Task.create(body="Hello world",
                           user=user,
                           start_ts=parse_dt("2017-07-24 13:50:00"))

        add(task)
        push()

        # REPL
        while True:
            # multiline prompt
            print ">",
            lines = []
            while True:
                line = raw_input()
                if line:
                    lines.append(line)
                    print ".",
                else:
                    break
            command = '\n'.join(lines)
            parse_command(command)
