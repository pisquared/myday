#!/usr/bin/env bash
./manage.py db migrate
for i in store/migrations/versions/*_.py; do
    sed -i 's/store.database.AwareDateTime(),/sa.DateTime(timezone=True),/' "$i";
done
rm store/migrations/versions/*.pyc