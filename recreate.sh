#!/bin/bash
sudo -u postgres psql --command "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'app' AND pid <> pg_backend_pid();"
sudo -u postgres psql --command "drop database app;"
sudo -u postgres psql --command "create database app;"
./manage.py db upgrade
./populate.py