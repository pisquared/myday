from store.database import db, ModelController, Deletable, Datable, AwareDateTime


class Task(ModelController, db.Model, Datable, Deletable):
    _STATUS_NOT_STARTED = "NOT_STARTED"
    _STATUS_IN_PROGRESS = "IN_PROGRESS"
    _STATUS_DONE = "DONE"

    body = db.Column(db.String)
    status = db.Column(db.String, default=_STATUS_NOT_STARTED)

    in_progress_ts = db.Column(AwareDateTime())
    done_ts = db.Column(AwareDateTime())

    start_ts = db.Column(AwareDateTime())
    due_ts = db.Column(AwareDateTime())

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='tasks')

    @property
    def is_done(self):
        return self.status == self._STATUS_DONE

    @property
    def is_running(self):
        return self.status == self._STATUS_IN_PROGRESS

    def __repr__(self):
        return '<Task %r>' % self.id
