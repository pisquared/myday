from flask_security import UserMixin, RoleMixin

from store.database import db, ModelController, Deletable, Datable


class LatestCUD(ModelController, db.Model, Datable, Deletable):
    model_name = db.Column(db.String(80))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='latest_cuds')


roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(ModelController, db.Model, RoleMixin):
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '<Role %r>' % self.email


class User(ModelController, db.Model, UserMixin, Deletable):
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))

    name = db.Column(db.String)
    image_url = db.Column(db.String)
    tz_offset_seconds = db.Column(db.Integer, default=0)

    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return '<User %r>' % self.email


class GeneralSettings(ModelController, db.Model):
    panels = db.Column(db.JSON)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='general_settings')
