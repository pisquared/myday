from flask_login import current_user

from store import database
from store.database import db, ModelController, Deletable, Datable, AwareDateTime
from webapp.lib import get_now_user, format_seconds_to_human


class Activity(ModelController, db.Model, Datable, Deletable):
    name = db.Column(db.String)
    icon = db.Column(db.String)

    is_running = db.Column(db.Boolean, default=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='activities')

    def get_running_session(self):
        return ActivitySession.get_one_by(user=self.user,
                                          activity=self,
                                          status=ActivitySession._STATUS_IN_PROGRESS)

    def get_current_duration_sec(self):
        if self.is_running:
            session = self.get_running_session()
            now = get_now_user(current_user)
            return (now - session.start_ts).seconds
        return 0

    def create_journal_entry(self, activity, activity_session, now):
        from webapp.apps.journal_entry.model_view import journal_entry_vm
        duration = (activity_session.end_ts - activity_session.start_ts).seconds
        duration = format_seconds_to_human(duration)
        journal_entry_body = "Activity {activity_name} for {duration}.".format(activity_name=activity.name,
                                                                               duration=duration)
        journal_entry_vm.add_create({'body': journal_entry_body})

    def start(self, start_type=None):
        if self.is_running:
            raise Exception("Activity already started")
        start_type = start_type or ActivitySession._START_TYPE_MANUAL
        now = get_now_user(current_user)
        running_session = ActivitySession.create(user=current_user,
                                                 activity=self,
                                                 created_ts=now,
                                                 start_type=start_type,
                                                 start_ts=now)
        self.is_running = True
        database.add(self)
        database.add(running_session)
        database.push()

    def stop(self):
        running_session = self.get_running_session()
        if not running_session:
            raise Exception("Activity not started.")
        now = get_now_user(current_user)
        database.update(running_session,
                        end_ts=now,
                        updated_ts=now,
                        status=ActivitySession._STATUS_DONE)
        self.create_journal_entry(self, running_session, now)
        self.is_running = False
        database.add(self)
        database.push()

    def __repr__(self):
        return '<Activity %r>' % self.name


class ActivitySession(ModelController, db.Model, Datable, Deletable):
    _STATUS_IN_PROGRESS = "IN_PROGRESS"
    _STATUS_DONE = "DONE"

    _START_TYPE_AUTOMATIC = "AUTOMATIC"
    _START_TYPE_MANUAL = "MANUAL"

    status = db.Column(db.String, default=_STATUS_IN_PROGRESS)
    start_ts = db.Column(AwareDateTime())
    end_ts = db.Column(AwareDateTime())

    start_type = db.Column(db.String, default=_START_TYPE_MANUAL)
    comment = db.Column(db.String)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref='sessions')

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='activity_sessions')

    @property
    def duration(self):
        if self.start_ts and self.end_ts:
            return (self.end_ts - self.start_ts).seconds
        elif self.start_ts and not self.end_ts:
            now = get_now_user(current_user)
            return (now - self.start_ts).seconds
        return 0

    def serialize_flat(self):
        serialized = super(ActivitySession, self).serialize_flat()
        if self.activity:
            serialized['activity'] = self.activity.serialize_flat()
        return serialized

    def __repr__(self):
        return '<ActivitySession %r>' % self.id


class ActivityReport(ModelController, db.Model, Datable, Deletable):
    _PERIOD_DAY = "DAY"
    _PERIOD_WEEK = "WEEK"

    # period start/end
    start_ts = db.Column(AwareDateTime())
    end_ts = db.Column(AwareDateTime())
    period = db.Column(db.String, default=_PERIOD_DAY)

    duration = db.Column(db.Integer, default=0)

    activity_id = db.Column(db.Integer, db.ForeignKey('activity.id'))
    activity = db.relationship("Activity", backref='reports')

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='activity_reports')

    def __repr__(self):
        return '<ActivityReport %r>' % self.id
