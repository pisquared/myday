from collections import OrderedDict

from sqlalchemy.orm import backref

from store.database import db, ModelController, Deletable, Datable


class JournalEntry(ModelController, db.Model, Datable, Deletable):
    body = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='journal_entries')

    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    task = db.relationship("Task", uselist=False, backref=backref('journal_entry', uselist=False))

    @staticmethod
    def get_grouped_by_date(user, start_ts=None, end_ts=None):
        filtered_js = JournalEntry.query \
            .filter(JournalEntry.user == user, JournalEntry.deleted == False)
        if start_ts and end_ts:
            filtered_js = filtered_js.filter(JournalEntry.created_ts >= start_ts, JournalEntry.created_ts < end_ts)
        all_je = filtered_js \
            .order_by(JournalEntry.created_ts.desc()).all()
        tupled_je = [(je.created_ts.strftime('%m %B'), je) for je in all_je]
        grouped_je = OrderedDict()
        for dt, je in tupled_je:
            if dt not in grouped_je:
                grouped_je[dt] = []
            grouped_je[dt].append(je)
        return grouped_je

    def serialize_flat(self):
        serialized = super(JournalEntry, self).serialize_flat()
        if self.task:
            serialized['task'] = self.task.serialize_flat()
        return serialized

    def __repr__(self):
        return '<JournalEntry %r>' % self.id
