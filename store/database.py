"""
This module provides communication with local database.
"""
import json

import datetime
from dateutil import tz
from flask import url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user
from flask_sqlalchemy import SQLAlchemy, Model
from flask_sqlalchemy_cache import CachingQuery
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.collections import InstrumentedList
from werkzeug.exceptions import abort
from werkzeug.utils import redirect

from webapp.config import Constants
from webapp.lib import get_now_utc, camel_case_to_snake_case

Model.query_class = CachingQuery
db = SQLAlchemy(query_class=CachingQuery)


def create_db():
    db.create_all()


def drop_db():
    db.session.remove()
    db.drop_all()


def update(model, **kwargs):
    for k, w in kwargs.iteritems():
        setattr(model, k, w)
    db.session.add(model)
    return model


def add(instance):
    db.session.add(instance)


def delete(instance):
    now = get_now_utc()
    instance.deleted = True
    instance.deleted_ts = now
    db.session.add(instance)


def push():
    db.session.commit()


class ModelController(object):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance

    @classmethod
    def _filter_method(cls, filters=None, **kwargs):
        if not filters:
            filters = []
        if hasattr(cls, "deleted"):
            kwargs['deleted'] = kwargs.get('deleted', False)
        # noinspection PyUnresolvedReferences
        return cls.query.filter_by(**kwargs).filter(*filters)

    @classmethod
    def get_one(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_by(cls, first=0, last=None, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()[first:last]

    @classmethod
    def get_one_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_all_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.get_one_by(**kwargs)
        if not instance:
            instance = cls.create(**kwargs)
            add(instance)
        return instance

    def serialize_flat(self):
        """
        Serializes object to dict
        It will ignore fields that are not encodable (set them to 'None').

        It doesn't auto-expand relations (since this could lead to self-references, and loop forever).
        :return:
        """
        if isinstance(self.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(self) if not x.startswith('_') and x != 'metadata']:
                data = self.__getattribute__(field)
                try:
                    if isinstance(data, datetime.datetime):
                        data = str(data)
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    if isinstance(data, InstrumentedList):
                        continue  # pragma: no cover
                    fields[field] = data
                except TypeError:
                    pass  # Don't assign anything
            # a json-encodable dict
            return fields


class AwareDateTime(db.TypeDecorator):
    """
    Results returned as aware datetimes, not naive ones.
    """

    impl = db.DateTime

    def process_result_value(self, value, dialect):
        if current_user and current_user.is_authenticated and value:
            tzinfo = tz.tzoffset(None, datetime.timedelta(seconds=current_user.tz_offset_seconds))
            return value.astimezone(tzinfo)
        return value


class Deletable(object):
    """
    This interface adds deletable properties to a model.
    """
    deleted = db.Column(db.Boolean, default=False)
    deleted_ts = db.Column(AwareDateTime())

    def undelete(self):
        self.deleted = False
        self.deleted_ts = None
        db.session.add(self)


class Datable(object):
    created_ts = db.Column(AwareDateTime())
    updated_ts = db.Column(AwareDateTime())


class AdminModelView(ModelView):
    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role(Constants.ADMIN_ROLE):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
