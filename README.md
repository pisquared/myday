# MyDay
Personal Organizer and Activity Tracker platform.

MyDay helps you align your long term goals with your daily Todos. It will use Tasks, Events (Calendars), Journal, Notes, Activities and other apps that can be configured and plugged as required.

MyDay will be open for various trackers to log your various activities such as running, listening to music or eating, all in a single system.

It is partially insipired by [a hacker news thread](https://news.ycombinator.com/item?id=13855577
), [Getting Things Done](http://gettingthingsdone.com/), [Objectives and Key Results](https://weekdone.com/resources/objectives-key-results) and other productivity systems.

## Changelog
### Version 1.0
* **SUMMARY:** apps: `Task`, `Journal Entry`
    * View `journal entries` and `tasks` by day using a calendar view
    * Create a task from journal entry by prefixing with `Todo:` or `Task:`
    * `Task` create, update, delete, run, done
    * `Journal entry` create, update, delete
    * Register/Login

## To install:

1. You need [vagrant](https://www.vagrantup.com/) and [virtualbox](https://www.virtualbox.org/) on your host system. The app runs in an Ubuntu installation to help prevent polution of the host system with development environment vars.

2. Once you have these two installed and ready, rename the file `sensitive.py.sample` to `sensitive.py`

```
cp sensitivie.py.sample sensitive.py
```

And input your secret authentications.

3. Finally, bring the machine up

```
vagrant up
```

This will take some time the first time around as it has to download an Ubuntu image and set up the system. Next cycles it would just bring the machine up.

## Development:

You need to enter the virtual machine first:

```
vagrant ssh
```

And run the server:
```
./manage.py runserver
```

To run with front end debugging (i.e. not minifying scripts):

```
./manage.py runserver --debug-fe=True
```

### Applying migrations
If you change any of the models in `store/models/*` you need to create a new migration of the database:

```
./db_migrate.sh
```

We are using this script rather than `manage.py db migrate` as it needs to replace `Datetime` fields with custom timezone aware class.

To upgrade the database to the latest version:

```
./manage.py db upgrade
```

## Production

1. Modify and copy the `provisioning/production.conf` to `/etc/nginx/sites-available`

```
sudo cp provisioning/production.conf /etc/nginx/sites-available/production.conf
```

2. Generate a ceritificate using [Let's encrypt's certbot for ubuntu xenial and nginx](https://certbot.eff.org/#ubuntuxenial-nginx)

3. Start a screen session:
```
screen
```

Pass `production` argument after runserver

```
./manage.py runserver production
```

Exit the screen session with `Ctrl+A` and then press `D`.


## Software and tools used
What follows is a description of the programming languages, tools and services used in the development.

| Type | Name |
|---|---|
| Programming languages | [python](https://www.python.org/), HTML, [Jinja2](http://jinja.pocoo.org/), JavaScript, CSS, [Bash](https://www.gnu.org/software/bash/manual/bashref.html), [Markdown](https://en.wikipedia.org/wiki/Markdown) |
| IDE | [PyCharm](https://www.jetbrains.com/pycharm/) |
| Reproducible environments | [Vagrant](https://www.vagrantup.com/) |
| Virtualization | [Virtualbox](https://www.virtualbox.org/) |
| Operating system | [Linux Ubuntu 16.04 LTS](http://releases.ubuntu.com/16.04/) |
| Version control | [Git](https://git-scm.com/) |
| Code repository | [GitLab](https://gitlab.com/) |
| Issue management | [GitLab](https://gitlab.com/) |
| Web app framework | [Flask](http://flask.pocoo.org) |
| Web app framework plugins | [Flask-Security](https://pythonhosted.org/Flask-Security/), [Flask-SocketIO](https://flask-socketio.readthedocs.io/en/latest/), [Flask-SQLAlchemy](http://flask-sqlalchemy.pocoo.org/2.1/), [Flask-Cache](https://pythonhosted.org/Flask-Cache/), [Flask-Admin](https://flask-admin.readthedocs.io/en/latest/), [Flask-Assets](https://flask-assets.readthedocs.io/en/latest/), [Flask-DebugToolbar](https://flask-debugtoolbar.readthedocs.io/en/latest/), [Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/), [Flask-SQLAlchemy-Cache](https://github.com/iurisilvio/Flask-SQLAlchemy-Cache), [Flask-Uploads](https://pythonhosted.org/Flask-Uploads/), [Flask-WTF](https://flask-wtf.readthedocs.io/en/stable/), |
| Python libraries | [dateutil](https://dateutil.readthedocs.io/en/stable/), [dateparser](https://dateparser.readthedocs.io/en/latest/), [eventlet](http://eventlet.net/), [inflection](https://pypi.python.org/pypi/inflection), [psycopg2](http://initd.org/psycopg/) |
| HTML Templating | [Jinja2](http://jinja.pocoo.org/) |
| CSS Framework | [Materialize](http://materializecss.com/) |
| JavaScript Lib & Frameworks | [JQuery](http://jquery.com/), [Underscore](http://underscorejs.org/), [Backbone](http://backbonejs.org/), [Backbone LocalStorage](https://github.com/jeromegn/Backbone.localStorage), [MomentJs](http://momentjs.com/), [Bootstrap calendar](https://github.com/Serhioromano/bootstrap-calendar), [Socket-io](https://socket.io/) |
| Database | [PostgreSQL](https://www.postgresql.org/) |
| Database Admin | [PgAdmin](https://www.pgadmin.org/) |
| ORM | [SQLAlchemy](http://www.sqlalchemy.org/) |
| Webserver | [ngninx](https://nginx.org/en/docs/) |
| DNS | [Hover](https://www.hover.com/) |
| HTTPS | [Let's encrypt](https://letsencrypt.org/) |
